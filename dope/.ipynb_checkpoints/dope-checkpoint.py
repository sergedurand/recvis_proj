# Copyright 2020-present NAVER Corp.
# CC BY-NC-SA 4.0
# Available only for non-commercial use

import sys, os
import argparse
import os.path as osp

import torchvision
from PIL import Image
# import cv2
# import matplotlib.pyplot as plt
import numpy as np

import torch
from torchvision.transforms import ToTensor
from pathlib import Path

_thisdir = Path(__file__).parent
sys.path.append(str(_thisdir.parent))

from dope.dope_model import dope_resnet50, num_joints
from dope import postprocess

from dope import visu
import matplotlib.pyplot as plt


def dope(imagename, modelname, postprocessing='ppi', do_visu2d=False, do_visu3d=False):
    if postprocessing == 'ppi':
        sys.path.append(str(_thisdir) + '/lcrnet-v2-improved-ppi/')
        try:
            from lcr_net_ppi_improved import LCRNet_PPI_improved
        except ModuleNotFoundError:
            raise Exception(
                'To use the pose proposals integration (ppi) as postprocessing, please follow the readme instruction by cloning our modified version of LCRNet_v2.0 here. Alternatively, you can use --postprocess nms without any installation, with a slight decrease of performance.')

    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

    # load model
    ckpt_fname = osp.join(_thisdir, 'models', modelname + '.pth.tgz')
    if not os.path.isfile(ckpt_fname):
        raise Exception(
            '{:s} does not exist, please download the model first and place it in the models/ folder'.format(
                ckpt_fname))
    ckpt = torch.load(ckpt_fname, map_location=device)
    ckpt['half'] = False  # uncomment this line in case your device cannot handle half computation
    ckpt['dope_kwargs']['rpn_post_nms_top_n_test'] = 1000
    model = dope_resnet50(**ckpt['dope_kwargs'])
    if ckpt['half']: model = model.half()
    model = model.eval()
    model.load_state_dict(ckpt['state_dict'])
    model = model.to(device)

    # load the image
    print('Loading image', imagename)
    image = Image.open(imagename)
    imlist = [ToTensor()(image).to(device)]
    if ckpt['half']: imlist = [im.half() for im in imlist]
    resolution = imlist[0].size()[-2:]
    print(len(imlist))
    print(imlist[0].shape)

    # forward pass of the dope network
    print('Running DOPE')
    with torch.no_grad():
        results = model(imlist, None)[0]
    # print(type(results))
    # print("results = ",results)

    # postprocess results (pose proposals integration, wrists/head assignment)
    print('Postprocessing')
    assert postprocessing in ['nms', 'ppi']
    parts = ['body', 'hand', 'face']
    if postprocessing == 'ppi':
        res = {k: v.float().data.cpu().numpy() for k, v in results.items()}
        detections = {}
        for part in parts:
            detections[part] = LCRNet_PPI_improved(res[part + '_scores'], res['boxes'], res[part + '_pose2d'],
                                                   res[part + '_pose3d'], resolution, **ckpt[part + '_ppi_kwargs'])
    else:  # nms
        detections = {}
        for part in parts:
            print("Part = ", part)
            print(results[part + "_scores"].shape)
            dets, indices, bestcls = postprocess.DOPE_NMS(results[part + '_scores'], results['boxes'],
                                                          results[part + '_pose2d'], results[part + '_pose3d'],
                                                          min_score=0.0)
            dets = {k: v.float().data.cpu().numpy() for k, v in dets.items()}
            detections[part] = [
                {'score': dets['score'][i], 'pose2d': dets['pose2d'][i, ...], 'pose3d': dets['pose3d'][i, ...]} for i in
                range(dets['score'].size)]
            if part == 'hand':
                for i in range(len(detections[part])):
                    detections[part][i]['hand_isright'] = bestcls < ckpt['hand_ppi_kwargs']['K']

    # assignment of hands and head to body
    detections, body_with_wrists, body_with_head = postprocess.assign_hands_and_head_to_body(detections)

    # display results
    if do_visu2d:
        print('Displaying results')
        det_poses2d = {
            part: np.stack([d['pose2d'] for d in part_detections], axis=0) if len(part_detections) > 0 else np.empty(
                (0, num_joints[part], 2), dtype=np.float32) for part, part_detections in detections.items()}
        scores = {part: [d['score'] for d in part_detections] for part, part_detections in detections.items()}
        imout = visu.visualize_bodyhandface2d(np.asarray(image)[:, :, ::-1],
                                              det_poses2d,
                                              dict_scores=scores,
                                              )
        outfile = imagename + '_{:s}.jpg'.format(modelname)
        # plt.imsave(outfile, imout)
        print('\t', outfile)

    # display results in 3D
    if do_visu3d:
        print('Displaying results in 3D')
        import visu3d
        viewer3d = visu3d.Viewer3d()
        img3d, img2d = viewer3d.plot3d(image,
                                       bodies={'pose3d': np.stack([d['pose3d'] for d in detections['body']]),
                                               'pose2d': np.stack([d['pose2d'] for d in detections['body']])},
                                       hands={'pose3d': np.stack([d['pose3d'] for d in detections['hand']]),
                                              'pose2d': np.stack([d['pose2d'] for d in detections['hand']])},
                                       faces={'pose3d': np.stack([d['pose3d'] for d in detections['face']]),
                                              'pose2d': np.stack([d['pose2d'] for d in detections['face']])},
                                       body_with_wrists=body_with_wrists,
                                       body_with_head=body_with_head,
                                       interactive=False)
        outfile3d = imagename + '_{:s}_visu3d.jpg'.format(modelname)
        # plt.imsave(outfile3d, img3d[:, :, ::-1])
        print('\t', outfile3d)


def load_video(videopath):
    vid = torchvision.io.read_video(
        filename=videopath, pts_unit='sec')[0]
    frames = [ToTensor()(frame.numpy()) for frame in vid]
    return frames, vid


def get_features(videoname, model, ckpt, postprocessing='ppi', save_2d=False, save_3d=False, savedir="test/"):
    if postprocessing in ["ppi","both"]:
        sys.path.append(str(_thisdir) + '/lcrnet-v2-improved-ppi/')
        try:
            from lcr_net_ppi_improved import LCRNet_PPI_improved
        except ModuleNotFoundError:
            raise Exception(
                'To use the pose proposals integration (ppi) as postprocessing, please follow the readme instruction by cloning our modified version of LCRNet_v2.0 here. Alternatively, you can use --postprocess nms without any installation, with a slight decrease of performance.')

    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    # load the frames
    imlist, vid = load_video(videoname)
    # print("Video has {} frames".format(len(imlist)))
    imlist = [frame.to(device) for frame in imlist]

    if ckpt['half']: imlist = [im.half() for im in imlist]
    resolution = imlist[0].size()[-2:]
    # print('Running DOPE')
    with torch.no_grad():
        results = [model([img], None)[0] for img in imlist]
        # postprocess results (pose proposals integration, wrists/head assignment)
    # print('Postprocessing')
    assert postprocessing in ['nms', 'ppi', 'both']
    parts = ['body', 'hand', 'face']
    detect_list_ppi = []
    detect_list_nms = []
    ckpt["hand_ppi_kwargs"]["th_persondetect"] /= 13
    if postprocessing == 'ppi' or postprocessing == 'both':
        for i in range(len(results)):
            res = {k: v.float().data.cpu().numpy() for k, v in results[i].items()}
            detections = {}
            for part in parts:
                detections[part] = LCRNet_PPI_improved(res[part + '_scores'], res['boxes'], res[part + '_pose2d'],
                                                       res[part + '_pose3d'], resolution,
                                                       **ckpt[part + '_ppi_kwargs'])
            for k, v in detections.items():
                if len(v) > 1 and k == "hand":
                    detections[k] = v[:1]
            detect_list_ppi.append(detections)
    if postprocessing == "nms" or postprocessing == "both":  # nms
        for i in range(len(results)):
            detections = {}
            for part in parts:
                if part == "hand":
                    # on Phoenix14T dataset it's particularly hard to detect hands (because of blurriness due to movements is my guess)
                    min_score = 0.01
                else:
                    min_score = 0.3
                res = postprocess.DOPE_NMS(results[i][part + '_scores'], results[i]['boxes'],
                                           results[i][part + '_pose2d'],
                                           results[i][part + '_pose3d'],
                                           min_score=min_score)
                if len(res) != 3:
                    detections[part] = []
                else:
                    dets, indices, bestcls = res
                    dets = {k: v.float().data.cpu().numpy() for k, v in dets.items()}
                    detections[part] = [
                        {'score': dets['score'][i], 'pose2d': dets['pose2d'][i, ...], 'pose3d': dets['pose3d'][i, ...]}
                        for
                        i in
                        range(dets['score'].size)]
                if part == 'hand':
                    for i in range(len(detections[part])):
                        detections[part][i]['hand_isright'] = bestcls < ckpt['hand_ppi_kwargs']['K']
                for k, v in detections.items():
                    if len(v) > 1:
                        detections[k] = v[:1]
            detect_list_nms.append(detections)
    if save_2d or save_3d:
        if not Path(savedir).is_dir():
            Path(savedir).mkdir()
    if postprocessing == "ppi":
        detect_list = detect_list_ppi
    elif postprocessing == "nms":
        detect_list = detect_list_nms
    else:
        detect_list = detect_list_ppi
    if save_2d:

        H, W = vid.shape[1], vid.shape[2]
        video = list()
        for i in range(len(detect_list)):
            detections = detect_list[i]
            image = imlist[i].cpu().permute(1, 2, 0).numpy()
            detections, body_with_wrists, body_with_head = postprocess.assign_hands_and_head_to_body(detections)
            det_poses2d = {
                part: np.stack([d['pose2d'] for d in part_detections], axis=0) if len(
                    part_detections) > 0 else np.empty(
                    (0, num_joints[part], 2), dtype=np.float32) for part, part_detections in detections.items()}
            scores = {part: [d['score'] for d in part_detections] for part, part_detections in detections.items()}
            imout = visu.visualize_bodyhandface2d(np.asarray(255 * image),
                                                  det_poses2d,
                                                  dict_scores=scores,
                                                  )
            video.append(torch.tensor(imout[:H, :W, :].copy()))
        video = torch.stack(video)
        savename = savedir + Path(videoname).stem + "_{}_".format(postprocessing) + "_pose.mp4"
        torchvision.io.write_video(savename, video, fps=25)
    if save_3d:
        import visu3d
        video = list()
        for i in range(len(detect_list)):
            detections = detect_list[i]
            detections, body_with_wrists, body_with_head = postprocess.assign_hands_and_head_to_body(detections)
            H, W = vid.shape[1], vid.shape[2]
            image = imlist[i]

            viewer3d = visu3d.Viewer3d()
            img3d, img2d = viewer3d.plot3d(image,
                                           bodies={'pose3d': np.array([]) if len(detections['body']) == 0
                                                    else np.stack([d['pose3d'] for d in detections['body']]),
                                                   'pose2d': np.array([]) if len(detections['body']) == 0
                                                   else np.stack([d['pose2d'] for d in detections['body']])},
                                           hands={'pose3d': np.array([]) if len(detections['hand']) == 0
                                                   else np.stack([d['pose3d'] for d in detections['hand']]),
                                                  'pose2d': np.array([]) if len(detections['hand']) == 0
                                                  else np.stack([d['pose2d'] for d in detections['hand']])},
                                           faces={'pose3d': np.array([]) if len(detections['face']) == 0 else
                                           np.stack([d['pose3d'] for d in detections['face']]),
                                                  'pose2d': np.array([]) if len(detections['face']) == 0
                                                  else np.stack([d['pose2d'] for d in detections['face']])},
                                           body_with_wrists=body_with_wrists,
                                           body_with_head=body_with_head,
                                           interactive=False)
            _img3d = np.zeros(shape=(1280, 1280, 3))
            _img3d[:img3d.shape[0], :img3d.shape[1], :] = img3d
            video.append(torch.tensor(_img3d))
        video = torch.stack(video)
        outfile3d = savedir + Path(videoname).stem + "_visu3d_{}_".format(postprocessing) + "_pose.mp4"
        torchvision.io.write_video(outfile3d, video, fps=25)

    if postprocessing == "ppi":
        return results, None, detect_list_nms
    elif postprocessing == "nms":
        return results, detect_list_ppi, None
    else:
        return results, detect_list_nms, detect_list_ppi


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='running DOPE on an image: python dope.py --model <modelname> --image <imagename>')
    parser.add_argument('--model', required=True, type=str, help='name of the model to use (eg DOPE_v1_0_0)')
    parser.add_argument('--image', required=True, type=str, help='path to the image')
    parser.add_argument('--postprocess', default='ppi', choices=['ppi', 'nms'], help='postprocessing method')
    parser.add_argument('--visu3d', dest='do_visu3d', default=False, action='store_true')
    args = parser.parse_args()
    dope(args.image, args.model, postprocessing=args.postprocess, do_visu3d=args.do_visu3d)
