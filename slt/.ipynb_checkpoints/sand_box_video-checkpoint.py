import torchvision
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np
from PIL import Image
from torchvision.transforms import ToTensor
import torch

# from dope import dope, _thisdir

VIDEO_DIR = "/home/serge/Documents/recvis_proj/slt/videos/videos/train"
video_path = Path(VIDEO_DIR + '/01April_2010_Thursday_heute-6694.mp4')
print(video_path.is_file())
print(video_path.stem)

vid1 = torchvision.io.read_video(filename=str(video_path),pts_unit='sec')
img = np.asarray(vid1[0])
print(vid1[0][0].shape)
save_dir = Path(video_path.stem)
# save_dir.mkdir()
# for i in range(vid1[0].shape[0]):
#     save_path = save_dir.joinpath(Path(f"{i}.jpg"))
#     img = vid1[0][i]
#     img = np.rot90(m=img.permute(1,0,2).numpy(),k=-1,axes=(0,1))
#     plt.imsave(str(save_path),img)

# DOPE_DIR = "../dope/"
# model_dir = DOPE_DIR + "models"
# model = "DOPErealtime_v1_0_0"
# image = str(save_dir) + "/0.jpg"
# # dope(imagename=image,modelname=model,postprocessing="nms",do_visu3d=False)
# print('Loading image', image)
# image = Image.open(image)
# imlist = [ToTensor()(image)]
# print("PIL loading + toTensor", imlist[0].shape)
# image = imlist[0]
# print(image.shape)
# print(image[0][0])
# vid_to_tensor = ToTensor()(vid1[0][0].numpy())
# print(torch.eq(image,vid_to_tensor))
# def load_video(videopath):
#     vid = torchvision.io.read_video(
#         filename=videopath, pts_unit='sec')[0]
#     vid = [ToTensor()(frame.numpy()) for frame in vid]
#     return vid
#
# vid = load_video(str(video_path))
# print(len(vid))
# print(vid[0].shape)