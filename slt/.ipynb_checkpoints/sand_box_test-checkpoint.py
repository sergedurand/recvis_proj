from time import time
from pathlib import Path

import torchvision
from pathlib import Path

from slt.signjoey.data import load_data
from slt.signjoey.helpers import load_config
import torch
this_dir = Path(__file__).parent
import sys
sys.path.append(str(this_dir.parent.parent)) # project dir is parent of parent of current file

cfg = load_config(str(this_dir.parent) + "/slt/configs/sign.yaml")
start = time()
data = load_data(cfg["data"])
elapsed = time() - start
print("Loading data ={:.2f} s".format(elapsed))

125,129,130
t1 = torch.zeros(130,32,32,3)
t2 = torch.zeros(130,32,32,3)
t3 = torch.zeros(130,32,32,3)
t4 = torch.zeros(130,32,32,3)

t1[:128] = torch.randn(128,32,32,3)
t2 = torch.randn(130,32,32,3)
t3[:120] = torch.randn(120,32,32,3)
t3[:124] = torch.randn(124,32,32,3)

batch = torch.stack([t1,t2,t3,t4])
print(batch.shape)
model = load_backbone()