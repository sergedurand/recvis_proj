"""
Additional utility functions to combine dope and slt
"""
from pathlib import Path

import mpu

this_dir = Path(__file__).parent
import sys
sys.path.append(str(this_dir.parent))

import torchvision

from dope.dope_model import dope_resnet50, num_joints
import torch


"""
pose estimation data is structured as
name: str
    frames: list (size = nb of frames)
        frame: dict
            body: 
            hand:
            face:
        
"""
def load_pose_estim(filename: str):
    data = mpu.io.read(filename)
    name = Path(filename).stem
    data = data[name]
    nb_frames = len(data)
    # for i in range(nb_frames):
    #     for k, v in data[i].items():
    #         if v.shape[0] > 4:
    #             print(k)
    #             print(v.shape[0])
    return data

def load_batch_video(videopath):
    vid = torchvision.io.read_video(filename=str(videopath), pts_unit='sec')[0]
    vid = ToTensor()(vid.permute(0, 3, 1, 2))
    return vid

# from time import time
# start = time()
# for path in Path("train/").glob("*.pickle"):
#     data = load_pose_estim(str(path))
#
# elapsed = time()-start
# print("Took {:.2f} s".format(elapsed))
dope_path = Path(__file__).parent.parent.joinpath(Path("dope/"))


def load_dope_backbone(modelname):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    ckpt_fname = Path(__file__).parent.parent.joinpath(Path("dope/models/{}.pth.tgz".format(modelname)))
    if not ckpt_fname.is_file():
        raise Exception(
            '{:s} does not exist, please download the model first and place it in the models/ folder'.format(
                str(ckpt_fname)))
    print('Loading model', modelname)
    ckpt = torch.load(ckpt_fname, map_location=device)
    ckpt['half'] = False  # uncomment this line in case your device cannot handle half computation
    ckpt['dope_kwargs']['rpn_post_nms_top_n_test'] = 1000
    model = dope_resnet50(**ckpt['dope_kwargs'])
    if ckpt['half']: model = model.half()
    model = model.eval()
    model.load_state_dict(ckpt['state_dict'])
    model = model.to(device)
    return model.backbone.resnet_backbone


class ToTensor:
    """Applies the :class:`~torchvision.transforms.ToTensor` transform to a batch of images.
    Code from https://github.com/pratogab/batch-transforms/blob/master/batch_transforms.py
    """

    def __init__(self):
        self.max = 255

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor of size (N, C, H, W) to be tensorized.
        Returns:
            Tensor: Tensorized Tensor.
        """
        return tensor.float().div_(self.max)


model = load_dope_backbone("DOPE_v1_0_0")
