import mpu
from pathlib import Path

data_paths = Path("../").glob("train*.pickle")
data = dict()
for path in data_paths:
    _data = mpu.io.read(str(path))
    data = {**data, **_data}
    
videos = [str(k) for k in data.keys()]
videos = list(set(videos))
print(len(videos))