import unittest
from pathlib import Path

import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict

_REQUIREMENTS_PATH = Path("./requirements.txt")
class TestRequirements(unittest.TestCase):
    """Test availability of required packages."""

    def test_requirements(self):
        """Test that each required package is available."""
        # Ref: https://stackoverflow.com/a/45474387/
        requirements = pkg_resources.parse_requirements(_REQUIREMENTS_PATH.open())
        for requirement in requirements:
            requirement = str(requirement)
            with self.subTest(requirement=requirement):
                try:
                    pkg_resources.require(requirement)
                except DistributionNotFound as e:
                    print(e)
                    continue
                except VersionConflict as e:
                    print("Version Conflict:")
                    print(e)
                    continue
                
test = TestRequirements()
test.test_requirements()
