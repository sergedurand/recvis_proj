from pathlib import Path
import yaml
from signjoey.helpers import load_config

configs = Path("new_yaml").glob("*.yaml")

for cfg in configs:
    stem = str(cfg.stem)
    cfg = load_config(str(cfg))
    cfg["training"]["recognition_loss_weight"] = 1.0
    
    recognition_loss_weight: 5.0
    translation_loss_weight: 1.0
