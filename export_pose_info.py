from pathlib import Path

from tqdm import tqdm
import numpy as np
from dope.dope import get_features
import torch

from dope.dope_model import dope_resnet50
from slt.add_utils import load_batch_video
import mpu

modelname = "DOPE_v1_0_0"
thisdir = Path(__file__).parent.resolve()

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

# load model
ckpt_fname = str(thisdir) + '/dope/models/' + modelname + '.pth.tgz'
if not Path(ckpt_fname).is_file():
    raise Exception(
        '{:s} does not exist, please download the model first and place it in the models/ folder'.format(
            ckpt_fname))
print('Loading model', modelname)
ckpt = torch.load(ckpt_fname, map_location=device)
ckpt['half'] = False  # uncomment this line in case your device cannot handle half computation
ckpt['dope_kwargs']['rpn_post_nms_top_n_test'] = 1000
model = dope_resnet50(**ckpt['dope_kwargs'])
if ckpt['half']: model = model.half()
model = model.eval()
model.load_state_dict(ckpt['state_dict'])
model = model.to(device)

train_path = thisdir.joinpath("slt/videos/train/").glob("*.mp4")
train_path = list(train_path)
# min_len = 800
# min_vid = ""
# for path in train_path:
#     batch = load_batch_video(str(path))
#     nb_frame = batch.shape[0]
#     if nb_frame < min_len:
#         min_len = nb_frame
#         min_vid = str(path)
# print("shortest video is {} with {} frames".format(min_vid,min_len))

train_path = list(train_path)
train_path.sort()
nb_vid = 1000
nb_frames = 0
cpt_1 = 0
cpt_2 = 0
for i, idx in enumerate(tqdm(train_path, position=0, leave=True)):
    ppi = dict()
    nms = dict()
    vid = train_path[i]
    results, detect_list_nms, detect_list_ppi = get_features(str(vid),model=model,ckpt=ckpt,postprocessing="both",savedir="vid2dsamples",save_2d=False)
    nms[str(vid.parent.stem) + "/" + str(vid.name)] = detect_list_nms
    ppi[str(vid.parent.stem) + "/" + str(vid.name)] = detect_list_ppi
#     for i in range(len(detect_list_nms)):
#         nb_frames += 1
#         if len(detect_list_nms[i]["hand"]) == 0:
#             cpt_1 += 1
#         if len(detect_list_ppi[i]["hand"]) == 0:
#             cpt_2 += 1
    mpu.io.write("train_pose_info/nms_{}.pickle".format(str(vid.stem)),nms)
    mpu.io.write("train_pose_info/ppi_{}train.pickle".format(str(vid.stem)),ppi)


# nms = mpu.io.read("nms_2_samples.pickle")
# for k, v in nms.items():
#     d = v
#     for k, v in d[0].items():
#         print(k)
#         print(type(v))

#     break