from pathlib import Path

from tqdm import tqdm
import numpy as np
from dope import postprocess
import torchvision
import torch
import mpu

from slt.add_utils import load_batch_video

data = mpu.io.read("random_samples.pickle")
savedir = Path(__file__).parent.resolve()
for k, v in data.items():
    videoname = Path(k).stem
    videoname = "slt/videos/train/" + videoname + ".mp4"
    vid = load_batch_video(str(videoname))
    detect_list = v
    from dope import visu3d
    video = list()
    outfile3d = Path(videoname).stem + "_visu3d_{}_".format("ppi") + "_pose.mp4"
    if Path(outfile3d).is_file():
        continue
    for i,_  in enumerate(tqdm(detect_list,position=0,leave=True)):
        detections = detect_list[i]
        detections, body_with_wrists, body_with_head = postprocess.assign_hands_and_head_to_body(detections)
        image = vid[i].permute(1,2,0)
        viewer3d = visu3d.Viewer3d()
        img3d, img2d = viewer3d.plot3d(image,
                                       bodies={'pose3d': np.array([]) if len(detections['body']) == 0 else np.stack(
                                           [d['pose3d'] for d in detections['body']]),
                                               'pose2d': np.array([]) if len(detections['body']) == 0 else np.stack(
                                                   [d['pose2d'] for d in detections['body']])},
                                       hands={'pose3d': np.array([]) if len(detections['hand']) == 0 else np.stack(
                                           [d['pose3d'] for d in detections['hand']]),
                                              'pose2d': np.array([]) if len(detections['hand']) == 0 else np.stack(
                                                  [d['pose2d'] for d in detections['hand']])},
                                       faces={'pose3d': np.array([]) if len(detections['face']) == 0 else np.stack(
                                           [d['pose3d'] for d in detections['face']]),
                                              'pose2d': np.array([]) if len(detections['face']) == 0 else np.stack(
                                                  [d['pose2d'] for d in detections['face']])},
                                        body_with_wrists=body_with_wrists,
                                        body_with_head=body_with_head,
                                        interactive=False)
        if img3d.shape[0] != 720:
            print(img3d.shape)
        _img3d = np.zeros(shape=(1280,1280,3))
        _img3d[:img3d.shape[0],:img3d.shape[1],:] = img3d
        video.append(torch.tensor(_img3d))
    video = torch.stack(video)
    torchvision.io.write_video(outfile3d, video, fps=25)