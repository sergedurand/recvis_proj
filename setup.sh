#!/bin/bash
git clone https://gitlab.com/sergedurand/recvis_proj.git
cd recvis_proj/slt/
sh data/download.sh
unzip videos_b.zip
pip install asn1crypto --user
pip install cryptography --user
pip install Keras-Applications --user
pip install keyboard --user
pip install opencv-python==4.2.0.32 --user
pip install --upgrade Pillow --user
pip install pluggy==0.13.1 --user
pip install portalocker==1.5.2 --user
pip install pycosat --user
pip install pyOpenSSL --user
pip install pytz==2019.3 --user
pip install --upgrade PyYAML --user
pip install ruamel-yaml --user
pip install sentencepiece --user
pip install torchtext==0.5.0 --user
pip install urllib3==1.25.7 --user
pip install pytorch-warmup --user
pip install xmltodict --user
pip install sacrebleu --user
pip install natsort==7.0.1 --user
pip install plotly==4.5.0 --user
pip install pytest==5.4.0 --user
pip install typing --user
pip install av --user
