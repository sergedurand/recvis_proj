from pathlib import Path
import mpu

train_files = Path("train_pose_info/").glob("*.pickle")
res = dict()
cpt = 0
for path in train_files:
    vid_data = mpu.io.read(str(path))
    vid = path.stem
    if vid[:4] == "nms_":
        continue
    vid = vid[4:-5]
    vid = "train/" + vid
    res[vid] = vid_data[vid + ".mp4"]
    
mpu.io.write("ppi_train.pickle",res)